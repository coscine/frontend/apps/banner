import jQuery from 'jquery';
import BootstrapVue from 'bootstrap-vue';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import Vue from 'vue';
import BannerApp from './BannerApp.vue';
import VueI18n from 'vue-i18n';
import { LanguageUtil } from '@coscine/app-util';

import Vuex from 'vuex';
import store from '@coscine/vuex-store';

Vue.config.productionTip = false;

Vue.use(BootstrapVue);
Vue.use(VueI18n);
Vue.use(Vuex);

jQuery(() => {
    const i18n = new VueI18n({
      locale: LanguageUtil.getLanguage(),
      messages: coscine.i18n.banner,
      silentFallbackWarn: true,
    });
    new Vue({
      store,
      i18n,
      render: (h) => h(BannerApp),
    }).$mount('banner');
});


